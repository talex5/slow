let run_worker n =
  for _i = 1 to n * 10000000 do
    ignore (Sys.opaque_identity (ref ()))
  done

let spawn_child fn =
  match Unix.fork () with
  | 0 -> fn (); exit 0
  | child ->
    fun () ->
      match Unix.waitpid [] child with
      | _, WEXITED 0 -> ()
      | _ -> failwith "Child failed!"

let spawn_domain fn =
  let d = Domain.spawn fn in
  fun () -> Domain.join d

let main n_workers count fork =
  let before = Unix.gettimeofday () in
  let domains =
    let spawn = if fork then spawn_child else spawn_domain in
    List.init (n_workers - 1) (fun _ -> spawn (fun () -> run_worker count))
  in
  run_worker count;    (* Also do work in main domain *)
  List.iter (fun f -> f ()) domains;
  (Unix.gettimeofday () -. before) *. 1000.

let () =
  let n_workers =
    match Sys.argv with
    | [| _; n |] -> int_of_string n
    | [| _ |] -> 8
    | _ -> failwith "Usage: main.exe N_WORKERS"
  in
  let count = 20 in
  let processes_time = main n_workers count true in
  let domains_time = main n_workers count false in
  Format.printf "%d, %.0f, %.0f@." n_workers domains_time processes_time
